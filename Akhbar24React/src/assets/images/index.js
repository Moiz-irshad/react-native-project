const splash = require('./splash.png');
const icHome = require('./ic_home.png');
const logo = require('./logo.png');

export default {
    splash,
    icHome,
    logo
}