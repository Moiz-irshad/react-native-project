import { SET_EMPLOYEES } from '../actions';

const initialState = {
  employees: [],
};

export function employee(state = initialState, action) {
  switch (action.type) {
    case SET_EMPLOYEES:
      return {
        ...state,
        employees: action.employees,
      };
    default:
      return state;
  }
}
