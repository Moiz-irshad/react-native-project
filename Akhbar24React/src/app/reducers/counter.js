import {INCREMENT_COUNT, DECREMENT_COUNT, SET_COUNT} from '../actions/counter';

const initialState = {
  count: 0,
};

export function counter(state = initialState, action) {
  switch (action.type) {
    case INCREMENT_COUNT:
      return {
        ...state,
        count: state.count + 1,
      };
    case DECREMENT_COUNT:
      return {
        ...state,
        count: state.count - 1,
      };
    case SET_COUNT:
      return {
        ...state,
        count: action.value,
      };
    default:
      return state;
  }
}
