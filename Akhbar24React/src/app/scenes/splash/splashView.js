import React from 'react';
import { connect } from 'react-redux';
import {ImageBackground, ActivityIndicator, Text} from 'react-native';
import Images from '../../../assets/images';
import styles from './style';

class SplashView extends React.PureComponent {
  render() {
    const {
      count
    } = this.props;

    return (
      <ImageBackground resizeMode={'cover'} source={Images.splash} style={styles.container}>
        <Text style={{fontSize: 20, color: 'red'}}>{count}</Text>
        <ActivityIndicator style={{ position: 'absolute', bottom: 20, alignSelf: 'center' }} color="red" />
      </ImageBackground>
    );
  }
}

const mapStateToProps = ( { counter }) => {
  return {
    count: counter.count
  }
}

export default connect(mapStateToProps, null)(SplashView);
