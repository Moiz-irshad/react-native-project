import React from 'react';
import {View} from 'react-native';
import SplashView from './splashView';
import * as Navigator from '../../services/navigationService';

class SplashContainer extends React.PureComponent {

    componentDidMount() {
        setTimeout(() => {
            Navigator.navigate('Home');
        }, 3000);
    }

  render() {
    return (
      <SplashView />
    );
  }
}

export default SplashContainer;


/*
<View
        style={{
          flex: 1,
          backgroundColor: 'green',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 1,
            height: 100,
            backgroundColor: 'red',
          }}
        />

        <View
          style={{
            width: 100,
            height: 100,
            backgroundColor: 'yellow',
          }}
        />
      </View>
*/
