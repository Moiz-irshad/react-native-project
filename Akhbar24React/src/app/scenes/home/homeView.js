import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Platform,
  Dimensions,
  FlatList,
} from 'react-native';
import styles from './style';
import NavigationBar from '../../components/navigationBar';
import images from '../../../assets/images';
import {
  incrementCounter,
  decrementCounter,
  setCounter,
  getEmployees
} from '../../actions';

const text = Platform.OS === 'ios' ? 'ios' : 'android';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class HomeView extends React.PureComponent {
  hide = false;

  performPromise(a) {
    return new Promise((resolve, reject) => {
      if (a === 2) {
        resolve('a is 2');
      } else {
        reject('a was not 2');
      }
    });
  }

  renderItem({item: {employee_name}}) {
    return (
      <View
        style={{
          width: '100%',
          height: 70,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>{employee_name}</Text>
      </View>
    );
  }

  componentDidMount() {
    // this.performPromise(3).then( res => {
    //   console.warn(res);
    // }).catch( err => {
    //   console.warn(err);
    // })

    const {asyncGetEmployees} = this.props;
    asyncGetEmployees();
  }

  render() {
    const {onMenuOpen, count, employees} = this.props;

    return (
      <View style={styles.container}>
        <NavigationBar
          onPressLeft={onMenuOpen}
          titleView={
            <Image
              resizeMode={'contain'}
              source={images.logo}
              style={{height: 40}}
            />
          }
          leftText={height}
        />
        <FlatList
          style={{flex: 1}}
          data={employees}
          renderItem={this.renderItem}
        />

        {this.hide && (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 30,
              }}>
              {count}
            </Text>

            <View
              style={{
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                onPress={() => this.props.incrementKro()}
                style={{marginRight: 10}}>
                <Text style={{fontSize: 17}}>Increment</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.props.decrementKro()}>
                <Text style={{fontSize: 17}}>Decrement</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.props.resetKro(20)}>
                <Text style={{fontSize: 17}}>Reset</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.props.onNavigate}>
                <Text style={{fontSize: 17}}>Navigate</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = ({counter, employee}) => {
  return {
    count: counter.count,
    employees: employee.employees
  };
};

const mapDispatchToProps = dispatch => {
  return {
    incrementKro: () => dispatch(incrementCounter()),
    decrementKro: () => dispatch(decrementCounter()),
    resetKro: val => dispatch(setCounter(val)),
    asyncGetEmployees: () => dispatch(getEmployees()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeView);
