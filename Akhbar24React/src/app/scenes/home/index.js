import React from 'react';
import HomeView from './homeView';
import { Image} from 'react-native';
import * as Navigator from '../../services/navigationService';
import Images from '../../../assets/images';

class HomeContainer extends React.PureComponent {

  static navigationOptions = {
    drawerLabel: 'Notifications',
    drawerIcon: ({ tintColor }) => (
      <Image
        source={Images.logo}
        style={[{ width: 20, height: 20}, { tintColor: tintColor }]}
      />
    ),
  };

  constructor(props) {
    super(props);

    const {
      navigation
    } = props;

    const age = navigation.getParam('age', 0);
    console.warn(age);
  }


  render() {
    return (
      <HomeView 
        onNavigate={ () => this.props.navigation.push('Splash')}
        onMenuOpen={() => Navigator.openDrawer()} 
      />
    );
  }
}

export default HomeContainer;


/*
<View
        style={{
          flex: 1,
          backgroundColor: 'green',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 1,
            height: 100,
            backgroundColor: 'red',
          }}
        />

        <View
          style={{
            width: 100,
            height: 100,
            backgroundColor: 'yellow',
          }}
        />
      </View>
*/
