import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems: 'center'
    },
    logo: {
        width: '100%',
        height: '100%'
    }
})