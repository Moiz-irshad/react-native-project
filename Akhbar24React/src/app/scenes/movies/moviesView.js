import React from 'react';
import { View, FlatList } from 'react-native';

const renderItem = ({ item }) => (
    <View style={{
        width: '100%',
        height: 60,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    }}>
        <Text>
            {item.title}
        </Text>
    </View>
)

const MoviesView = () => (
    <View style={{ flex: 1}}>
        <FlatList 
            data={[]}
            renderItem={renderItem}
        />
    </View>
);

export default MoviesView;