import React from 'react';
import {Provider} from 'react-redux';
import MainStack from './routes/stackNavigator';
import {setContainer} from './services/navigationService';
import store from './store';
import LoadingView from './components/loadingView';

const App = () => (
  <Provider store={store}>
    <LoadingView>
      <MainStack
        ref={ref => {
          setContainer(ref);
        }}
      />
    </LoadingView>
  </Provider>
);

export default App;
