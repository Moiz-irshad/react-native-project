import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

const NavigationBar = ({
  title,
  leftText,
  rightText,
  onPressLeft,
  onPressRight,
  style,
  leftTitleStyle,
  rightTitleStyle,
  titleView
}) => (
  <View style={[styles.container, style]}>
    <TouchableOpacity onPress={onPressLeft ? () => onPressLeft('hello hasan') : undefined} activeOpacity={0.5}>
      <Text style={[styles.btnTitle, leftTitleStyle]}>{leftText}</Text>
    </TouchableOpacity>

    {titleView || <Text style={styles.title}>{title}</Text>}

    <TouchableOpacity onPress={onPressRight} activeOpacity={0.5}>
      <Text style={[styles.btnTitle, rightTitleStyle]}>{rightText}</Text>
    </TouchableOpacity>
  </View>
);

export default NavigationBar;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ddd",
    width: "100%",
    height: 50,
    marginTop: 50,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 10
  },
  logo: {
    height: 30
  },
  title: {
    color: "white",
    fontSize: 20
  },
  btnTitle: {
    fontSize: 17
  }
});
