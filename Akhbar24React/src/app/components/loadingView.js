import React from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

const LoadingView = ({loading, children}) => (
  <View style={{flex: 1}}>
    {children}
    <Spinner style={{flex: 1}} visible={loading} />
  </View>
);

const mapStateToProps = ({loader}) => ({
  loading: loader.loading,
});

export default connect(
  mapStateToProps,
  null,
)(LoadingView);
