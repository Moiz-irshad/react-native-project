import store from '../store';
import {hideLoader, showLoader} from '../actions';

export function fetchData(url, method = 'GET', body = null) {
  store.dispatch(showLoader());
  return fetch(url, {
    method: method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: body,
  })
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(error => {
      store.dispatch(hideLoader());
    });
}
