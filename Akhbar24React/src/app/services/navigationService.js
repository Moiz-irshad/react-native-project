/* eslint-disable no-underscore-dangle */
import { NavigationActions, StackActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { from } from 'rxjs';

let _container;
let _currentScreen;
let _drawer;

export function setContainer(container) {
    _container = container;
}

export function setDrawer(drawer) {
    _drawer = drawer;
    drawer.blockSwipeAbleDrawer(true);
}

export function navigate(routeName, params = null) {
    _container
    && _container.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        }),
    );
}

export function replaceRoute(route, params = null) {
    _container
    && _container.dispatch({
        key: route,
        type: 'ReplaceCurrentScreen',
        routeName: route,
        params: params,
    });
}

export function back() {
    _container
    && _container.dispatch(NavigationActions.back());
}

export function openDrawer() {
    _container
    && _container.dispatch(DrawerActions.openDrawer());
    // _drawer
    // && _drawer.open();
}

export function closeDrawer() {
    _drawer
    && _drawer.close();
}


export function push(routeName, params = null) {
    if (_container && _container.props.navigation) {
        _container.props.navigation.push(routeName, params);
    }
}

export function reset(routeName, params) {
    //console.warn(_container);
    _container && _container.dispatch(
        StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName,
                    params,
                }),
            ],
        }),
    );
}

export function getCurrentScreen() {
    return _currentScreen;
}

function getActiveRouteName(navigationState) {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
        return getActiveRouteName(route);
    }
    return route.routeName;
}

export function onNavigationStateChange(prevState, currentState) {
    const currentScreen = getActiveRouteName(currentState);
    const prevScreen = getActiveRouteName(prevState);

    if (prevScreen !== currentScreen) {
        _currentScreen = currentScreen;
    }
}
