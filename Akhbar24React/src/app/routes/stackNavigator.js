import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Splash from '../scenes/splash';
import Home from './drawerNavigator';

const MainStack = createStackNavigator({
    Splash: { screen: Splash },
    Home: { screen: Home }
}, {
    initialRouteName: 'Splash',
    headerMode: { visible: false },
    gesturesEnabled: false,
});

export default createAppContainer(MainStack);