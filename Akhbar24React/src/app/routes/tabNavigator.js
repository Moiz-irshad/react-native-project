import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Images from '../../assets/images';

import Home from '../scenes/home';

const TabNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={Images.icHome}
                    resizeMode={'contain'}
                    style={{tintColor: tintColor, width: 20, height: 20}}
                />
            )
        })
    },
    Profile: {
        screen: Home,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={Images.icHome}
                    resizeMode={'contain'}
                    style={{tintColor: tintColor, width: 20, height: 20}}
                />
            )
        })
    },
    Albums: {
        screen: Home,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={Images.icHome}
                    resizeMode={'contain'}
                    style={{tintColor: tintColor, width: 20, height: 20}}
                />
            )
        })
    },
},{
    tabBarOptions: {
        style: { backgroundColor: '#ddd', paddingBottom: 5, paddingTop: 5, height: 50},
        activeTintColor: 'red',
        inactiveTintColor: 'green'
    }
});

export default TabNavigator;
