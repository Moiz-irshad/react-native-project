import { fetchData } from '../services/api';
import {
    showLoader,
    hideLoader
} from './loader';

export const SET_EMPLOYEES = 'SET_EMPLOYEES';
const setEmployees = (employees) => ({
    type: SET_EMPLOYEES,
    employees
});


export const getEmployees = () => dispatch => {
    fetchData('http://dummy.restapiexample.com/api/v1/employees')
    .then( employees => {
        dispatch(hideLoader())
        dispatch(setEmployees(employees))
    })
}
