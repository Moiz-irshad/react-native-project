import React from "react";
import {
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity
} from "react-native";

const abc = () => ({
  a: "sdsd"
});

const UserView = ({
  title,
  leftText,
  rightText,
  onPressLeft,
  onPressRight
}) => (
  <View
    style={{
      backgroundColor: "green",
      width: "100%",
      height: 50,
      marginTop: 50,
      justifyContent: "space-between",
      alignItems: "center",
      flexDirection: "row",
      paddingHorizontal: 10
    }}
  >
    <TouchableOpacity onPress={onPressLeft} activeOpacity={0.5}>
      <Text style={{ fontSize: 17 }}>{leftText}</Text>
    </TouchableOpacity>

    <Text
      style={{
        color: "white",
        fontSize: 20
      }}
    >
      {title}
    </Text>

    <TouchableOpacity onPress={onPressRight} activeOpacity={0.5}>
      <Text style={{ fontSize: 17 }}>{rightText}</Text>
    </TouchableOpacity>
  </View>
);

export default UserView;
