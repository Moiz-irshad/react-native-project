import React from "react";
import {
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import styles from './styles';

const abc = () => ({
  a: "sdsd"
});

const UserView = ({
  title,
  leftText,
  rightText,
  onPressLeft,
  onPressRight,
  style,
  leftTitleStyle,
  rightTitleStyle,
  titleView
}) => (
  <View style={[styles.container, style]}>
    <TouchableOpacity onPress={onPressLeft ? () => onPressLeft('hello hasan') : undefined} activeOpacity={0.5}>
      <Text style={[styles.btnTitle, leftTitleStyle]}>{leftText}</Text>
    </TouchableOpacity>

    {titleView || <Text style={styles.title}>{title}</Text>}

    <TouchableOpacity onPress={onPressRight} activeOpacity={0.5}>
      <Text style={[styles.btnTitle, rightTitleStyle]}>{rightText}</Text>
    </TouchableOpacity>
  </View>
);

export default UserView;