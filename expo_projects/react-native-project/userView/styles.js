import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
      backgroundColor: "red",
      width: "100%",
      height: 50,
      marginTop: 50,
      justifyContent: "space-between",
      alignItems: "center",
      flexDirection: "row",
      paddingHorizontal: 10
    },
    logo: {
      height: 30
    },
    title: {
      color: "white",
      fontSize: 20
    },
    btnTitle: {
      fontSize: 17
    }
  });
  