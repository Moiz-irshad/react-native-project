import React from "react";
import { TouchableOpacity, Text, StyleSheet, ActivityIndicator } from "react-native";

const CustomButton = ({ onPress, loading }) => (
    <TouchableOpacity disabled={loading} onPress={onPress} style={[styles.container, loading ? styles.disabledStyle : null]}>
      {
          loading ? <ActivityIndicator color={'white'} /> : <Text style={styles.title}>Login</Text>
      }
    </TouchableOpacity>
)

export default CustomButton;

const styles = StyleSheet.create({
  container: {
    width: 150,
    height: 40,
    borderRadius: 5,
    backgroundColor: "green",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "white",
    fontSize: 17
  },
  disabledStyle: {
      backgroundColor: '#DDD'
  }
});
