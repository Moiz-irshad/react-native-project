import React from 'react';
import { View } from 'react-native';
import CustomButton from './Button';

export default class StateFullView extends React.PureComponent
{
    constructor(props){
        super(props);

        this.state = {
            loading: false
        }
    }

    toggleLoading() {
        this.setState( prevState => ({
            loading: !prevState.loading
        }))
    }

    onButtonClick() {
        this.toggleLoading();
        setTimeout(() => {
            this.toggleLoading()
        }, 3000)
    }

    render() {
        const {
            loading
        } = this.state;

        return(
            <View>
                <CustomButton onPress={() => this.onButtonClick()} loading={loading} />
            </View>
        )
    }
}
