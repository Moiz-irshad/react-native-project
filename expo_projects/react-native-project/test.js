function sum(a,b) {
    return a+b;
}

function sub(a,b) {
    return a-b;
}

export default function mul(a,b) {
    return a*b;
}

function onPress(msg) {
    alert(msg)
}

export {
    sub,
    sum,
    onPress
}