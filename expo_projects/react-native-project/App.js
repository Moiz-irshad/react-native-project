import React from 'react';
import { StyleSheet, Text, View, Image, Alert } from 'react-native';
import UserView from './userView';
import { onPress } from './test';
import Images from './assets';
import CustomButton from './Button';
import StateFullView from './stateFullView';

const onPressLeft = (a,b) => {
  Alert.alert(a,b);
}

export default function App() {
  return ([
    <UserView
      style={{
        backgroundColor: '#DDD'
      }}
      leftTitleStyle={{ color: 'rgba(255,0,0,1)'}}
      title="Hello"
      leftText="Backkk"
      rightText="Share"
      onPressLeft={onPressLeft}
      onPressRight={() => alert('share')}
      titleView={<View style={{height: 50, justifyContent: 'center',
      alignItems: 'center'}}>
        <Image resizeMode='contain' style={{
        height: 20, width: 20,
      }} source={{ uri: 'https://cbsnews3.cbsistatic.com/hub/i/r/2010/12/03/79ed9c0e-a644-11e2-a3f0-029118418759/resize/620x465/94b66abc1317bbbd3581ad4f2c4bc7a7/365073.jpg'}} />
      <Text style={{ height: 20, fontSize: 13, color: 'red'}}>hi</Text>
      </View>}
    />,
    <StateFullView />
  ]
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
