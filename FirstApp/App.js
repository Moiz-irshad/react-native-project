/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, FlatList, SafeAreaView} from 'react-native';
import { AppRegistry, Image } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

import LocalizedStrings from 'react-native-localization';
import Test from "./Test";
import StateFullComp from "./StateFullComp";
import { Props } from "./Props.1";

// CommonJS syntax
// let LocalizedStrings  = require ('react-native-localization');

let strings = new LocalizedStrings({
    "en-US":{
        how:"How do you want your egg today?",
        boiledEgg:"Boiled egg",
        softBoiledEgg:"Soft-boiled egg",
        choice:"How to choose the egg"
    },
    en:{
        how:"How do you want your egg today?",
        boiledEgg:"Boiled egg",
        softBoiledEgg:"Soft-boiled egg",
        choice:"How to choose the egg"
    },
    it: {
        how:"Come vuoi il tuo uovo oggi?",
        boiledEgg:"Uovo sodo",
        softBoiledEgg:"Uovo alla coque",
        choice:"Come scegliere l'uovo"
    }
});

export default class App extends Component<Props> {

    constructor(props){
        super(props);

        strings.setLanguage('en');

        this.state = {
            refreshing: false
        }
    }


    onRefresh() {
        this.setState({
            refreshing: true
        })

        setTimeout(() => {
            this.setState({
                refreshing: false
            })
        }, 3000)
    }


  render() {

    const arr = ["Raza","Moiz","Mubashir","Wahab"];

      let pic = {
          uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
      };
      return (
          <SafeAreaView style={styles.container}>
            <Image source={pic} style={{width: 320, height: 110}}/>

            <View style={{alignItems : 'center'}}>
                <Text>Hello {arr[2]}! </Text>
            </View>
          </SafeAreaView>


      );

    // return (
    //   <SafeAreaView style={styles.container}>
    //     <FlatList
    //         onRefresh={() => this.onRefresh()}
    //         refreshing={this.state.refreshing}
    //         style={{ flex: 1}}
    //         data={arr}
    //         keyExtractor={item => item}
    //         renderItem={({item}) => (
    //             <View style={{flexDirection: 'column', flex: 1}}>
    //                 <Test  name={item} />
    //                 <StateFullComp name={item} />
    //             </View>
    //         )}
    //     />
    //   </SafeAreaView>
    // );
  }
}

/*

<TouchableOpacity key={item} onPress={() => alert(item)}>
                  <View style={{ flex: 1, borderWidth: 1, borderColor: 'red', borderRadius: 5, height: 50, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>{strings.how}</Text>
                  </View>
                </TouchableOpacity>
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'red'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
