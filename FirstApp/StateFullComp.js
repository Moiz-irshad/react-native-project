import React from 'react';
import {  View, Text, Image, TextInput, FlatList, ImageBackground} from 'react-native';


class StateFullComp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View style={{ flex: 1, backgroundColor: 'green', height: 100, marginBottom: 10, justifyContent: 'center', alignItems:'center'}}>
                <Text>{this.props.name}</Text>
            </View>
        )
    }
}

export default StateFullComp;