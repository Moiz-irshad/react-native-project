import React from 'react';
import {  View, Text, Image, TextInput, FlatList, ImageBackground} from 'react-native';

const Test = (props) => {
    return(
        <View style={{ flex: 1, backgroundColor: 'red', height: 100, justifyContent: 'center', alignItems:'center'}}>
            <Text>{props.name}</Text>
        </View>
    )
};

export default Test;
