import React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { View } from 'react-native';
import Home from '../scenes/home';
import DrawerView from '../components/drawerView';

const DrawerNavigator = createDrawerNavigator({
    Home: { screen: Home,},
    Profile: { screen: Home,},
    News: { screen: Home,},
    ContactUs: { screen: Home,},
    },{
        contentComponent: (props) => (<DrawerView />)
    }
);

export default DrawerNavigator;