import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Login from '../scenes/login';

const MainStack = createStackNavigator({
    Login: { screen: Login },
}, {
    initialRouteName: 'Login',
    headerMode: { visible: false },
    gesturesEnabled: false,
});

export default createAppContainer(MainStack);