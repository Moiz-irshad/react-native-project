import { combineReducers } from 'redux';
import { counter } from './counter';
import { loader } from './loader';
import { employee } from './employees';

export default combineReducers({
    counter,
    loader,
    employee
})