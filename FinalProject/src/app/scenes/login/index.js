import React from 'react';
import LoginView from './loginView';

class LoginViewContainer extends React.PureComponent
{
    render() {
        return(
            <LoginView 
                onSubmit={(data) => console.warn(data)}
            />
        )
    }
}
export default LoginViewContainer;
