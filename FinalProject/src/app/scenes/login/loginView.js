import React from 'react';
import * as Yup from 'yup';
import ImagePicker from 'react-native-image-picker';
import {withFormik} from 'formik';
import {View, StyleSheet, Image} from 'react-native';
import CustomTextField from '../../components/customTextField';
import CustomButton from '../../components/customButton';

const options = {
  title: 'Select Avatar',
  customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class LoginView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      image: null,
    };

    this.onOpenImagePicker = this.onOpenImagePicker.bind(this);
  }

  onOpenImagePicker() {
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        this.setState({
            image: source
        })

        console.log(response);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      }
    });
  }

  render() {
    const {values, errors, handleSubmit, setFieldValue} = this.props;
    const {image} = this.state;

    return (
      <View style={styles.container}>
        <Image
          source={image}
          resizeMode={'stretch'}
          style={{width: 100, height: 100, borderRadius: 50, marginBottom: 20}}
        />

        <CustomTextField
          value={values.email}
          error={errors.email}
          placeholder="Email"
          keyboardType={'email-address'}
          onChangeText={text => setFieldValue('email', text)}
        />

        <CustomTextField
          value={values.password}
          error={errors.password}
          style={{marginTop: 20}}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={text => setFieldValue('password', text)}
        />

        <CustomButton
          onPress={() => handleSubmit()}
          style={{marginTop: 20}}
          title="Login"
        />

        <CustomButton
          style={{marginTop: 20}}
          title="Select Image"
          onPress={this.onOpenImagePicker}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
});

const MyEnhancedForm = withFormik({
  mapPropsToValues: props => ({
    email: '',
    password: '',
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .required('field is required')
      .email('invalid email'),
    password: Yup.string()
      .required('password is required')
      .min(6, 'min is 6')
      .max(20, 'max is 20'),
  }),
  handleSubmit: (values, {props: {onSubmit}}) => {
    onSubmit(values);
  },
})(LoginView);

export default MyEnhancedForm;
