export const INCREMENT_COUNT = 'INCREMENT_COUNT';
export const DECREMENT_COUNT = 'DECREMENT_COUNT';
export const SET_COUNT = 'SET_COUNT';

export const incrementCounter = () => ({
    type: INCREMENT_COUNT
})

export const decrementCounter = () => ({
    type: DECREMENT_COUNT
})

export const setCounter = (value) => ({
    type: SET_COUNT,
    value
})