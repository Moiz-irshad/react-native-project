import React from 'react';
import {SafeAreaView} from 'react-navigation';
import {TouchableOpacity, StyleSheet, FlatList, Text} from 'react-native';

const DrawerView = props => {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={['Employee', 'Movies']}
        renderItem={({item}) => (
          <TouchableOpacity style={styles.row}>
            <Text style={styles.title}>{item}</Text>
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );
};

export default DrawerView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  row: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    marginBottom: 5,
    justifyContent: 'center'
  },
  title: {
    color: 'black',
    fontSize: 17,
    marginLeft: 20,
  },
});
