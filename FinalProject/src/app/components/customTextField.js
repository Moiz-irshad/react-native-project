import React from 'react';
import {View, TextInput, StyleSheet, Text} from 'react-native';

class CustomTextField extends React.PureComponent {
  state = {
    isActive: false,
  };

  constructor(props) {
    super(props);

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onFocus() {
    const {onFocus} = this.props;

    this.setState({
      isActive: true,
    });

    if (onFocus) {
      onFocus();
    }
  }

  onBlur() {
    const {onBlur} = this.props;

    this.setState({
      isActive: false,
    });

    if (onBlur) {
      onBlur();
    }
  }

  renderError(error) {
    if (error) {
      return <Text style={styles.error}>{error}</Text>;
    }
    return null;
  }

  getBorderStyle(error) {
    const {isActive} = this.state;
    if (isActive && !error) {
      return {borderWidth: 1, borderColor: 'orange'};
    }
    if (error) {
      return {borderWidth: 1, borderColor: 'red'};
    }
    return {borderWidth: 0};
  }

  render() {
    const {isActive} = this.state;
    const {style, textFieldStyle, error, ...rest} = this.props;
    const borderStyle = this.getBorderStyle(error);

    return (
      <View style={{width: '100%'}}>
        <View style={[styles.container, borderStyle, style]}>
          <TextInput
            {...rest}
            style={[styles.textField, textFieldStyle]}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
          />
        </View>
        {this.renderError(error)}
      </View>
    );
  }
}

export default CustomTextField;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    backgroundColor: '#EEE',
    borderRadius: 25,
    paddingHorizontal: 20,
    borderColor: 'orange',
    borderWidth: 0,
  },
  textField: {
    flex: 1,
  },
  error: {
    color: 'red',
    fontSize: 12,
    width: '100%',
    paddingTop: 5,
    paddingLeft: 20,
  },
});
