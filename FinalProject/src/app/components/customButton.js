import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';

const CustomButton = ({title, onPress, style, titleStyle, disabled}) => (
  <TouchableOpacity
    disabled={disabled}
    onPress={onPress}
    style={[styles.container, style, disabled ? {opacity: 0.5} : {}]}>
    <Text style={[styles.titleStyle, titleStyle]}>{title}</Text>
  </TouchableOpacity>
);

export default CustomButton;

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    borderRadius: 25,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyle: {
    color: 'white',
    fontSize: 17,
  },
});
