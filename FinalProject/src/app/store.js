import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../app/reducers';

const initialState = {};
const middleware = [thunk];


export default createStore(reducers, initialState, applyMiddleware(...middleware));